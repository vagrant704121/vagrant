Vagrant.configure("2") do |config|
config.vm.box = "ubuntu/bionic64"
config.vm.network "forwarded_port", guest: 82, host: 82
config.vm.provision "shell", inline: <<-SHELL
sudo apt-get update
sudo apt-get install -y nginx
sudo sed -i 's/listen 80;/listen 82;/g' /etc/nginx/sites-available/default
sudo systemctl restart nginx
SHELL
end